# Un bot discord avec node.js et discord.js
Le but de ce tuto est d'expliquer comment développer un bot discord qui aura des fonctionnalité basiques, même si vous ne connaissez rien en programmation !

## Introduction à la programmation
Node.js est un langage de programmation serveur orienté objet.

### Decortication  des mots précedents
#### Serveur :
Node.js s'execute sur un serveur, et non au niveau d'un client, comme le fait le js. C'est à dire que pour faire tourner ce bot, il faut forcément un serveur, mais votre ordinateur peut le faire.

#### orienté objet
Un objet est une variable qui contient plein d'informations : par exemple, un objet qui contient un message sur discord.js contient les différrents informations suivantes : 
- l'auteur du message
- le contenu du message
- les réactions
- le salon du message

et plein d'autres choses ! Cela s'appelle des propriété (property). Mais on peut aussi faire une action sur l'objet ou faire des actions sur les propriété.
- delete()
- edit()


### Les différents principaux types de variable
Une variable peut-être :
- Un string (ou chaine de caractère) qui est définit des manières suivantes
```js
NomDeVariable = "Du texte"
NomDeVariable = 'Du texte'
```
L'avantage d'avoir de type de variable est pour pouvoir mettre des appostrophes ou des guillemets comme dans la phrase suivantes : L'auteur du message est nommé "Flo - Fan"
```js
NomDeVariable = "L'auteur du message est nommé" + '"Flo - Fan"'
```
Le plus permettant de relier 2 chaînes de caractères.

- Un int (ou entier) (qui est un nombre sans virgule) qui est définit de la manière suivante
```js
NomDeLaVariable = 5
```

- Un float (ou nombre flottant) (qui est un nombre avec virgule) qui est définit de la manière suivante
```js
NomDeLaVariable = 5,6
```

### Introduction à node.js
#### Executer un script
Pour executer un script de nom nomdufichier.js, il suffit d'écrire cette commande dans un terminal (sous windows, il suffit de chercher cmd dans la barre de recherche, puis se déplacer dans le dossier voulu grâce à cd)
```
node nomdufichier.js
```

#### Installer des "extensions" pour node.js (avec npm)
Pour installer des "extensions" pour node.js il suffit d'executer la commande suivante, avec le nom de paquet que vous pouvez trouver sur le site suivant [npmjs.org](http://www.npmjs.org/). Par exemple pour installer le paquet discord.js (:wink:) il suffit d'executer la commande suivante dans le dossier du bot
```
npm install discord.js
```
que l'on peut raccourcir de la manière suivante
```
npm i discord.js
```
#### Créer un script simple (HelloWorld.js)
Le premier programme que l'on fait dans la plus part des langages est nommé souvent HelloWorld. En node.js le fichier ressemble à ça :
```js
console.log("Hello World");
```
On enregistre ce code dans un fichier nommé HelloWorld.js, puis on execute le fichier grâce cette commande
```
node HelloWorld.js
```
Le résultat attendu dans la console resemble à ça
```
C:\User\RedGl0w\Documents\HelloWorld> node HelloWorld.js
Hello World
C:\User\RedGl0w\Documents\HelloWorld>
```
